package me.d3249.example.minimalcrud.repository;


import me.d3249.example.minimalcrud.model.database.Author;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

//https://www.baeldung.com/spring-data-repositories
public interface AuthorRepository extends Repository<Author, Long> {

    //Query method https://www.baeldung.com/spring-data-derived-queries
    Iterable<Author> findAllByFirstNameLikeAndLastNameLikeAllIgnoreCase(String firstName, String lastName);

    //Custom query https://www.baeldung.com/spring-data-jpa-query
    @Query("select b.author from Book b where b.title = :queryTitle")
    Optional<Author> findByBookTitle(@Param("queryTitle") String title);
}
