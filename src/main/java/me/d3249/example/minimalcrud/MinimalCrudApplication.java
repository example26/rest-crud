package me.d3249.example.minimalcrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinimalCrudApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinimalCrudApplication.class, args);
    }

}
