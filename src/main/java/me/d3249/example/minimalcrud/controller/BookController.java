package me.d3249.example.minimalcrud.controller;

import lombok.RequiredArgsConstructor;
import me.d3249.example.minimalcrud.exception.MissingDataException;
import me.d3249.example.minimalcrud.model.database.Book;
import me.d3249.example.minimalcrud.model.dto.BookData;
import me.d3249.example.minimalcrud.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

@RestController //https://www.baeldung.com/rest-with-spring-series
@RequestMapping("/book")
@RequiredArgsConstructor(onConstructor_ = {@Autowired}) //Lombok annotation + Spring annotation
//https://projectlombok.org/features/constructor
// https://www.baeldung.com/spring-autowire
public class BookController {
    private final BookService service;

    @PostMapping("")
    public Book addNewBook(@RequestBody BookData newBook) { //We use a DTO because it's a good way to sanitize any user input

        if (newBook.getAuthorName().isEmpty()) {
            throw new MissingDataException("authorName is required");
        }
        if (newBook.getAuthorLastName().isEmpty()) {
            throw new MissingDataException("authorLastName is required");
        }
        if (newBook.getBookISBN().isEmpty()) {
            throw new MissingDataException("bookIsbn is required");
        }
        if (newBook.getBookTitle().isEmpty()) {
            throw new MissingDataException("bookTitle is required");
        }

        return service.saveBook(newBook.getBookISBN(),
                newBook.getBookTitle(),
                newBook.getAuthorName(),
                newBook.getAuthorLastName());
    }

    @GetMapping("")
    public Page<Book> getAllBooks(@PageableDefault Pageable pageRequest) { //https://www.baeldung.com/spring-data-jpa-pagination-sorting , https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/web/PageableDefault.html
        return service.allBooks(pageRequest);
    }

    @PutMapping("/{isbn}")
    public Book updateBookTitle(@PathVariable String isbn, @RequestBody BookData newBook) {
        if (newBook.getBookTitle().isEmpty()) {
            throw new MissingDataException("bookTitle is required");
        }

        return service.updateTitle(isbn, newBook.getBookTitle());
    }

    @DeleteMapping("/{isbn}")
    public void deleteBook(@PathVariable String isbn) {
        service.deleteBook(isbn);
    }

}
