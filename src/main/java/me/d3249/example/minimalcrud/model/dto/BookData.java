package me.d3249.example.minimalcrud.model.dto;

import lombok.Data;

@Data //Lombok annotation to generate hashCode, equals, getters and setters https://projectlombok.org/features/Data
public class BookData {
    private String bookISBN;
    private String bookTitle;
    private String authorName;
    private String authorLastName;
}
