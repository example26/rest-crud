package me.d3249.example.minimalcrud.model.database;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data //Lombok annotation to generate hashCode, equals, getters and setters https://projectlombok.org/features/Data
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true) //Lombok annotations https://projectlombok.org/features/constructor
@Entity //JPA annotation https://www.baeldung.com/jpa-entities
public class Author {
    @Id
    @GeneratedValue
    private Long id;
    private String firstName;
    private String lastName;

    public Author(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
