package me.d3249.example.minimalcrud.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)//This instructs Spring Web to translate this exception to a 400 HTTP status
public class MissingDataException extends RuntimeException{

    public MissingDataException(String message) {
        super(message);
    }
}
