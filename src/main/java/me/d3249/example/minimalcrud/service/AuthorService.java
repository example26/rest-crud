package me.d3249.example.minimalcrud.service;

import lombok.RequiredArgsConstructor;
import me.d3249.example.minimalcrud.exception.ItemNotFoundException;
import me.d3249.example.minimalcrud.model.database.Author;
import me.d3249.example.minimalcrud.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service //Spring annotation https://www.baeldung.com/spring-component-repository-service
@RequiredArgsConstructor(onConstructor_ = {@Autowired}) //Lombok annotation + Spring annotation
//https://projectlombok.org/features/constructor
// https://www.baeldung.com/spring-autowire
public class AuthorService {
    private final AuthorRepository repository;

    public Iterable<Author> authorsByNameLike(String firstName, String lastName) {

        var formattedFirstName = String.format("%%%s%%", firstName); //For the use of Like we need to add the wildcards % to queried strings
        var formattedLastName = String.format("%%%s%%", lastName);  //Since we're using String format, we have to scape the % character


        return repository.findAllByFirstNameLikeAndLastNameLikeAllIgnoreCase(formattedFirstName, formattedLastName);
    }

    public Author authorOfBook(String bookTitle) {
        return repository.findByBookTitle(bookTitle).orElseThrow(() -> new ItemNotFoundException(format("Book [%s] not found", bookTitle)));
    }
}
