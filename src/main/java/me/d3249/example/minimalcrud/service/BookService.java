package me.d3249.example.minimalcrud.service;

import lombok.RequiredArgsConstructor;
import me.d3249.example.minimalcrud.exception.ItemNotFoundException;
import me.d3249.example.minimalcrud.model.database.Author;
import me.d3249.example.minimalcrud.model.database.Book;
import me.d3249.example.minimalcrud.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service //Spring annotation https://www.baeldung.com/spring-component-repository-service
@RequiredArgsConstructor(onConstructor_ = {@Autowired}) //Lombok annotation + Spring annotation
//https://projectlombok.org/features/constructor
// https://www.baeldung.com/spring-autowire
public class BookService {
    private final BookRepository repository;

    public Book saveBook(String isbn, String title, String authorName, String authorLastName) {

        var author = new Author(authorName, authorLastName); //var is available since Java 10
        var book = new Book(isbn, title, author);

        return repository.save(book);
    }

    public Page<Book> allBooks(Pageable pageRequest) {
        return repository.findAll(pageRequest);
    }

    public Book oneBook(String isbn) {
        return repository.findById(isbn).orElseThrow(() -> new ItemNotFoundException(format("There's no book with ISBN [%s]", isbn)));
    }

    public Book updateTitle(String isbn, String newTitle) {
        var book = oneBook(isbn);
        book.setTitle(newTitle);
        return repository.save(book);
    }

    public void deleteBook(String isbn) {
        repository.deleteById(isbn);
    }

}
